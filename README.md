# Estimating Food Delivery Times by Isatou Sanneh

If you have ever ordered food online using food ordering platforms like GrubHub, you will notice that they usually provide you with an estimated delivery or pick up time.  In this tutorial, we will be using Linear Regression in TensorFlow to estimate Food Delivey Times.
 

The tutorial is divided into the following sections:

 * Installing TensorFlow
 * Representing our Data
 * Building our Model
 * Training our Model
 * Validating our Model
 * Predicting Delivery Times


## Installing TensorFlow
We will be using TensorFlow with Python.

You can use the following links to install TensorFlow on your various operating systems:

 * [Linux](https://www.tensorflow.org/install/install_linux)
 * [Mac OS X](https://www.tensorflow.org/install/install_mac)
 * [Windows](https://www.tensorflow.org/install/install_windows)


## Representing our Data
In order to create a model to solve our problem (food delivery times), the first thing we have to take into account is our data.  We need to analyze our data in order to select suitable inputs for our model.

#### Here is a sample of the items in the menu of the restaurant and their preparation times:

```
Food Item			Average Food Preparation Time (Minutes)
	French Fries	 				2
	Burger							3
	Milkshake						1
	Gyro							5
```


The following data/information can be used to represent our inputs:

 * The time it takes to prepare each food item
 * The number of orders that need to be prepared before the current order is prepared
 * The distance between the restaurant and the place the order has to be delivered to





## Building our Model

Our model will be built using Linear Regression.   Linear Regression is used to model the relationship between two or more variables in the form of a straight line.

The inputs of our model will be:

 * Average time it will take to prepare current order
 * Distance from restaurant to delivery address.  This will be 0 if the order will be picked up from the restaurant
 * How busy the restaurant is: 0 will be used to indicate that the restaurant is not busy, and 1 to indicate that it is busy

```
    def build_model(self):
        features = [tf.contrib.layers.real_valued_column("", dimension=3)]

        # linear regression will be used to build our model
        self.model = tf.contrib.learn.LinearRegressor(feature_columns=features)
```



## Training our Model
We need to use past data from actual food delivery times in order to train our model.  Below is the sample data we will be using to train our model:


```
Avg.Prep Time (Mins)	Delivery dist (Miles)	Busy	Delivery 
10								5				 0			20
15								10				 0			30
12								2				 1		    20
19								5				 0			29
10								5				 1			23
5								3				 1			16	
7								8				 0			19
12								14				 1			34
19							 	4				 1			31
3								0				 0			5
```

```
food_delivery_history = {
    "features": [
        [10, 5, 0],
        [15, 10, 0],
        [12, 2, 1],
        [19, 5, 0],
        [10, 5, 1],
        [5, 3, 1],
        [7, 8, 0],
        [12, 14, 1],
        [19, 4, 1],
        [3, 0, 0]
    ],
    "delivery_times": [
        [20],
        [30],
        [20],
        [29],
        [23],
        [16],
        [19],
        [34],
        [31],
        [5]
    ]
}

```

We will use 70% of our data to train our model.

```
    def get_training_data(self):
        # divide data into training data and validation data
        # We will use about 70% of the data for training and
        # the remainder for validation.

        partition_index = int(0.7 * self.data_size)

        # training data
        training_input = tf.constant(self.food_delivery_history["features"][:partition_index])
        training_output = tf.constant(self.food_delivery_history["delivery_times"][:partition_index])

        return training_input, training_output


    def train_model(self):
        # train model with the training data
        self.model = self.model.fit(input_fn=self.get_training_data, steps=75)
```


## Validating our Model
We will use 30% of our data for validation.  Validation helps us evaluate the accuracy of our model and make any neccesary changes to improve accuracy.

```
    def get_validation_data(self):
        # validation data
        partition_index = int(0.7 * self.data_size)

        # validation data
        validation_input = tf.constant(self.food_delivery_history["features"][partition_index:])
        validation_output = tf.constant(self.food_delivery_history["delivery_times"][partition_index:])

        return validation_input, validation_output

    def validate_model(self):
        # validate/evaluate model with the validation data

        # Validation helps us evaluate the accuracy of our model

        results = self.model.evaluate(input_fn=self.get_validation_data, steps=1)

        print("Loss: %f") %results["loss"]

        return results["loss"]
```


## Predicting Delivery Times

We can now use our model to estimate the time it will take to prepare and deliver a customer's order.

```
    def get_input(self, prep_time, dist_to_location, busy):
        return tf.constant([[prep_time, dist_to_location, busy]])

    def estimate_delivery_time(self, prep_time, dist_to_location, busy):
        predictions = self.model.predict(input_fn=lambda: self.get_input(prep_time, dist_to_location, busy))
        print(list(predictions))
```

Running our code
```
food_delivery = FoodDeliveryEstimate(food_delivery_history)
food_delivery.build_model()
food_delivery.train_model()
food_delivery.validate_model()
food_delivery.estimate_delivery_time(10, 5, 0)
food_delivery.estimate_delivery_time(25, 10, 1)
food_delivery.estimate_delivery_time(4, 7, 0)
```

Output
```
Loss: 0.070874
Input: [10, 5, 0] Output: [19.360682]
Input: [25, 10, 1] Output: [45.108288]
Input: [4, 7, 0] Output: [14.56367]
```