import tensorflow as tf
import numpy as np
from tensorflow.contrib.learn.python import SKCompat

# food delivery history

food_delivery_history = {
    "features": [
        [10, 5, 0],
        [15, 10, 0],
        [12, 2, 1],
        [19, 5, 0],
        [10, 5, 1],
        [5, 3, 1],
        [7, 8, 0],
        [12, 14, 1],
        [19, 4, 1],
        [3, 0, 0]
    ],
    "delivery_times": [
        [20],
        [30],
        [20],
        [29],
        [23],
        [16],
        [19],
        [34],
        [31],
        [5]
    ]
}

class FoodDeliveryEstimate():

    def __init__(self, food_delivery_history):
        self.food_delivery_history = food_delivery_history
        self.model = None
        self.data_size = len(self.food_delivery_history["features"])

    def build_model(self):
        features = [tf.contrib.layers.real_valued_column("", dimension=3)]

        # linear regression will be used to build our model
        self.model = tf.contrib.learn.LinearRegressor(feature_columns=features) 

    def get_training_data(self):
        # divide data into training data and validation data
        # We will use about 70% of the data for training and
        # the remainder for validation. 

        partition_index = int(0.7 * self.data_size)

        # training data
        training_input = tf.constant(self.food_delivery_history["features"][:partition_index])
        training_output = tf.constant(self.food_delivery_history["delivery_times"][:partition_index])

        return training_input, training_output


    def train_model(self):
        # train model with the training data
        self.model = self.model.fit(input_fn=self.get_training_data, steps=75)

    def get_validation_data(self):
        # validation data
        partition_index = int(0.7 * self.data_size)

        # validation data
        validation_input = tf.constant(self.food_delivery_history["features"][partition_index:])
        validation_output = tf.constant(self.food_delivery_history["delivery_times"][partition_index:])

        return validation_input, validation_output

    def validate_model(self):
        # validate/evaluate model with the validation data

        # Validation helps us evaluate the accuracy of our model

        results = self.model.evaluate(input_fn=self.get_validation_data, steps=1)

        print("Loss: %f") %results["loss"]
        
        return results["loss"]

    def get_input(self, prep_time, dist_to_location, total_item_prep):
        return tf.constant([[prep_time, dist_to_location, total_item_prep]])

    def estimate_delivery_time(self, prep_time, dist_to_location, total_item_prep): 
        predictions = self.model.predict(input_fn=lambda: self.get_input(prep_time, dist_to_location, total_item_prep))
        print "Input:", [prep_time, dist_to_location, total_item_prep], "Output:", list(predictions)
        #print("Output: ", list(predictions))
        #print(len(list(predictions)))
        #print(tf.reshape(list(predictions), []))
        #return (predictions[0])


food_delivery = FoodDeliveryEstimate(food_delivery_history)
food_delivery.build_model()
food_delivery.train_model()
food_delivery.validate_model()
food_delivery.estimate_delivery_time(10, 5, 0)
food_delivery.estimate_delivery_time(25, 10, 1)
food_delivery.estimate_delivery_time(4, 7, 0)
